FROM alpine:latest

RUN apk add --no-cache py-pip bash git openssh gettext libintl
RUN pip install --no-cache-dir docker-compose awscli